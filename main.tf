resource "azurerm_resource_group" "rg" {
  name     = "${local.env}_assignment_resource_group"
  location = "West Europe"
}

resource "azurerm_virtual_network" "vn" {
  name                = "${local.env}_assignment_virtual_network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

#SUB__1
resource "azurerm_subnet" "sub" {
  name                 = "${local.env}_assignment_sub"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vn.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "nwi" {
  count               = 2
  name                = "${local.env}_assignment_ni_${count.index}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "${local.env}_ip_config"
    subnet_id                     = azurerm_subnet.sub.id
    private_ip_address_allocation = "Dynamic"
  }
}


resource "azurerm_windows_virtual_machine" "vm" {
  count               = 2
  name                = "${local.env}-vm-${count.index}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  zone                = element([1, 2], count.index)

  size           = "Standard_B1s"
  admin_username = "adminuser"
  admin_password = "Password1234"

  network_interface_ids = [element(azurerm_network_interface.nwi.*.id, count.index)]

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-Datacenter"
    version   = "latest"
  }
}

resource "azurerm_storage_account" "st_acc" {
  name                     = "${local.env}082022sa"
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
  allow_blob_public_access = true
}

resource "azurerm_storage_container" "st_con" {
  name                  = "${local.env}-sc"
  storage_account_name  = "${local.env}082022sa"
  container_access_type = "blob"
  depends_on = [
    azurerm_storage_account.st_acc
  ]
}

# Here we are uploading our IIS Configuration script as a blob
# to the Azure storage account

resource "azurerm_storage_blob" "IIS_config" {
  name                   = "IIS_Config.ps1"
  storage_account_name   = "${local.env}082022sa"
  storage_container_name = azurerm_storage_container.st_con.name
  type                   = "Block"
  source                 = "IIS_Config.ps1"
  depends_on = [
    azurerm_storage_container.st_con
  ]
}

// This is the extension for appvm1
resource "azurerm_virtual_machine_extension" "vm_extension" {
  count                = 2
  name                 = "${local.env}-vm-extension${count.index}"
  virtual_machine_id   = element(azurerm_windows_virtual_machine.vm.*.id, count.index)
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.10"
  depends_on = [
    azurerm_storage_blob.IIS_config
  ]
  settings             = <<SETTINGS
    {
        "fileUris": ["https://${azurerm_storage_account.st_acc.name}.blob.core.windows.net/${local.env}-sc/IIS_Config.ps1"],
          "commandToExecute": "powershell -ExecutionPolicy Unrestricted -file IIS_Config.ps1"     
    }
SETTINGS
}

resource "azurerm_public_ip" "pub_ip" {
  name                = "${local.env}_assignment_machine_ip_for_lb"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_lb" "lb" {
  name                = "${local.env}_assignment_lb"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = "publicIPAddress"
    public_ip_address_id = azurerm_public_ip.pub_ip.id
  }
}

resource "azurerm_lb_backend_address_pool" "back" {
  loadbalancer_id = azurerm_lb.lb.id
  name            = "BackEndAddressPool"
}

resource "azurerm_lb_backend_address_pool_address" "vm_addrs" {
  count                   = 2
  name                    = "${local.env}-vm${count.index}"
  backend_address_pool_id = azurerm_lb_backend_address_pool.back.id
  virtual_network_id      = azurerm_virtual_network.vn.id
  ip_address              = element(azurerm_network_interface.nwi.*.private_ip_address, count.index)
}

resource "azurerm_lb_probe" "lb_probe" {
  loadbalancer_id     = azurerm_lb.lb.id
  resource_group_name = azurerm_resource_group.rg.name
  name                = "${local.env}-http-probe"
  port                = 80
}

resource "azurerm_lb_rule" "lb_rule" {
  resource_group_name            = azurerm_resource_group.rg.name
  loadbalancer_id                = azurerm_lb.lb.id
  name                           = "LBRule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "publicIPAddress"
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.back.id]
  probe_id                       = azurerm_lb_probe.lb_probe.id
}

resource "azurerm_network_security_group" "nsg" {
  name                = "app-nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "Allow_HTTP"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "nsg_association" {
  subnet_id                 = azurerm_subnet.sub.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}